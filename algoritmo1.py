import random
import time
import numpy

def criar():

    if n>0:
        matriz = []
        for i in range(n):
            linha = []
            for j in range(n):
                num = random.randint(1,11)
                linha.append(num)
            matriz.append(linha)

    return matriz
    
def mult(A,B):
    resul = []
    for linha in range(n):
        resul.append([])
        for coluna in range(n):
            resul[linha].append(0)
            for k in range(n):
                resul[linha][coluna] += A[linha][k] * B[k][coluna]
    return resul

if __name__ == '__main__':

    n = int(input("Informe o tamanho da matriz (n): "))
    a = int(input("Informe a quantidade de amostras (adequado <30): "))
    exp = []
    for i in range(a):
        inicio = time.time()
        A = criar()
        B = criar()
        print(mult(A,B))
        fim = time.time()
               
        exp.append(fim - inicio)
    
    print('Tempo de execução')
    print(exp)
    print('Média')
    print(numpy.mean(exp))
    print('Desvio Padrão')
    print(numpy.std(exp))