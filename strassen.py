import random
import time
import numpy

def mult(M1,M2):
    resul = []
    for linha in range(len(M1)):
        resul.append([])
        for coluna in range(len(M1)):
            resul[linha].append(0)
            for k in range(len(M1)):
                resul[linha][coluna] += M1[linha][k] * M2[k][coluna]
    return resul
    
def soma(M1,M2):
    resul = []
    
    for lin in range (len(M1)):
        l = []
        for col in range(len(M1)):
            l.append(M1[lin][col] + M2[lin][col])
        resul.append(l)
    return resul  

def sub(M1,M2):
    resul = []
    
    for lin in range (len(M1)):
        l = []
        for col in range(len(M1)):
            l.append(M1[lin][col] - M2[lin][col])
        resul.append(l)
    return resul    

def criar():

    if n>0:
        matriz = []
        for i in range(n):
            linha = []
            for j in range(n):
                num = random.randint(1,11)
                linha.append(num)
            matriz.append(linha)

    return matriz
      
def strassen(M1,M2):
    
    A = []
    E = []
    for lin in range (0,int(n/2)):   #primeiro quadrante
        l1 = []
        l2 = []
        for col in range(0,int(n/2)):
            l1.append(M1[lin][col])
            l2.append(M2[lin][col])
        A.append(l1)
        E.append(l2)
    
    B = []
    F = []
    for lin in range (0,int(n/2)):   #segundo quadrante
        l1 = []
        l2 = []
        for col in range(int(n/2),n):
            l1.append(M1[lin][col])
            l2.append(M2[lin][col])
        B.append(l1)
        F.append(l2)
    
    C = []
    G = []
    for lin in range (int(n/2),n):   #terceiro quadrante
        l1 = []
        l2 = []
        for col in range(0,int(n/2)):
            l1.append(M1[lin][col])
            l2.append(M2[lin][col])
        C.append(l1)
        G.append(l2)
    
    D = []
    H = []
    for lin in range (int(n/2),n):   #quarto quadrante
        l1 = []
        l2 = []
        for col in range(int(n/2),n):
            l1.append(M1[lin][col])
            l2.append(M2[lin][col])
        D.append(l1)
        H.append(l2)
        
    P1 = mult(A,sub(F,H))
    P2 = mult(H,soma(A,B))
    P3 = mult(E,soma(C,D))
    P4 = mult(D,sub(G,E))
    P5 = mult(soma(A,D),soma(E,H))
    P6 = mult(sub(B,D),soma(G,H))
    P7 = mult(sub(A,C),soma(E,F))
    
    quadrante1 = soma(P5,sub(P4,soma(P2,P6)))
    quadrante2 = soma(P1,P2)
    quadrante3 = soma(P3,P4)
    quadrante4 = soma(P1,sub(P5,sub(P3,P7)))
    
    XY = []
    
    for lin in range(int(n/2)):
        l = []
        for col in range(int(n/2)):
            l.append(quadrante1[lin][col]) #preenchendo as linhas de XY
        for col in range(int(n/2)):
            l.append(quadrante2[lin][col])       
        XY.append(l)        
            
    for lin in range(int(n/2)):
        l = []
        for col in range(int(n/2)):
            l.append(quadrante3[lin][col])
        for col in range(int(n/2)):
            l.append(quadrante4[lin][col])
        XY.append(l)
    
    return XY 

if __name__ == '__main__':
    n = int(input("Informe o tamanho da matriz (n): "))
    a = int(input("Informe a quantidade de amostras (adequado <30): "))
    exp = []
    for i in range(a):
        inicio = time.time()
        M1 = criar()
        M2 = criar()      
        XY = strassen(M1,M2)
        print("XY")
        print(XY)
        fim = time.time()
        exp.append(fim - inicio)
    print('Tempo de execução')
    print(exp)
    print('Média')
    print(numpy.mean(exp))
    print('Desvio Padrão')
    print(numpy.std(exp))